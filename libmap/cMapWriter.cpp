#include "cMapWriter.h"
#include "../libcc/_string.h"
#include "../libcc/save.h"

#define ccc_md(_path_)system(("md " + cc::replace((_path_), "/", "\\")).c_str())
#define ccc_move(_name_, _path_move_)system(("move " + cc::replace(((_name_) + " " + (_path_move_)), "/", "\\")).c_str())
#define ccc_copy(_name_, _name_copy_)system(("copy " + cc::replace(((_name_) + " " + (_name_copy_)), "/", "\\")).c_str())

#ifdef max
#undef max
#endif // max

#ifdef min
#undef min
#endif // min

static const char* s_folderCut = "图块";
static const char* s_folderMask = "遮罩";
static const int c320 = cMapReader::c320, c240 = cMapReader::c240;

bool cMapWriter::init(const std::string& path, int mapid, cLabel* labelMapid, cLabel* labelDesc)
{
	if (!cSprite::init())
	{
		return false;
	}
	_path = path;
	_labelMapid = labelMapid;
	_labelMapid->setColor(s888::GREEN);

	_path = path + toString("%04d.map", _id = mapid);
	auto ptr = cMapReader::getData(_path);

	if (ptr == nullptr)
	{
		_labelMapid->setString(_path + "无法读取");
		return false;
	}
	_m.clear();
	if (!_m.load(ptr))
	{
		_labelMapid->setString(_path + "无法读取");
		return false;
	}
	_m.loadMask();


	mapid = gge::System_GetState(gge::GGE_STATE_INT::GGE_TEXTURESIZE);
	const auto& info = _m.info;
	while (info.mw / _smalltimes > mapid || info.mh / _smalltimes > mapid)
	{
		++_smalltimes;
	}

	if (_smalltimes > 1)
	{
		_labelMapid->setColor(s888::RED);
		_labelMapid->setString(cc::toString(_id) + " 超过显卡最大纹理尺寸,导出大图将格外耗费内存");
		_data = new uint[info.mw * info.mh];
		_dataMask = new uint[info.mw * info.mh];
		memset(_dataMask, 0, info.mw * info.mh * 4);
	}
	else
	{
		_labelMapid->setString(cc::toString(_id));
	}

	_texture = gge::Texture_Create(info.mw / _smalltimes, info.mh / _smalltimes);
	_textureMask = gge::Texture_Create(_texture->GetWidth(), _texture->GetHeight());
	auto ptrMask = _textureMask->Lock();
	memset(ptrMask, 0, _textureMask->GetWidth() * _textureMask->GetHeight() * 4);
	_textureMask->Unlock();
//	_texture->FillColor(0);
//	_textureMask->FillColor(0);
	cSprite::setTexture(_texture);
//	cSprite::setTextureRect(Rect(0, 0, _texture->GetWidth(), _texture->GetHeight())); // 到底要不要设置区域
	this->setContentSize(Size(_texture->GetWidth(), _texture->GetHeight()));

	_labelDesc = labelDesc;
	_labelDesc->setColor(s888::YELLOW);

	auto& image = _m.getImage(0, false);
	if(image.isValid())
	{
		_path = path + toString("%d/%s/", _id, s_folderCut);
		ccc_md(_path);
		_path = path + toString("%d/%s/", _id, s_folderMask);
		ccc_md(_path);
	}
	_path = path;
	scheduleUpdate();
	return true;
}

// 一个色点的类型 (暂时不能修改)
typedef unsigned short int PIXEL;
// 游戏库中使用的不压缩的位图结构
typedef struct {
	int w, h, pitch;      //位图宽和高以及每行实际字节数
	int cl, ct, cr, cb;    //位图剪裁矩形的左上角顶点坐标
	//剪裁矩形的宽和高
	PIXEL *line[1];     //创建位图时动态分配大小
} BMP;

bool cMapWriter::readJPG(int iBlock, bool save)
{
	const auto& info = _m.info;
	int x = (iBlock % info.bw) * c320;
	int y = (iBlock / info.bw) * c240;
	uint* ptr = _texture->Lock(false, x / _smalltimes, y / _smalltimes, std::min(c320, info.mw - x) / _smalltimes, std::min(c240, info.mh - y) / _smalltimes);
	auto& image = _m.getImage(iBlock, false);
	if (!image.isValid())
	{
		return false;
	}
	gge::ggeTexture *textureCut = gge::Texture_Load((char*)image.image, 0, image.imageSize);
//	image.~sImage();
	image.reset();
	if (!textureCut)
	{
// 		gge::ggeTexture *textureCut = gge::Texture_Create(c320, c240);
// 		uint* data = textureCut->Lock();
// 		auto bmp = Unpak_jpg((uchar*)point.ptr, /*jpg_size*/point.size);
// 		auto line = bmp->line[0];
// 		WasReader::sRGB c;
// 		for (int i = c320 * c240 - 1; i >= 0; --i)
// 		{
// 			c.color = line[i];
// 			data[i] = ccg::color2gge32(Color4B(c.r << 3, c.g << 2, c.b << 3, 0xff));
// 		}
// 		destroy_bitmap(bmp);
// 		textureCut->Unlock();
	}
	uint* data = textureCut->Lock();


	int ix, iy, iwData;
	for (int h = 0; h < c240; h += 1/*smalltimes*/)
	{
		iy = y + h;
		if (y / c240 == info.bh - 1 && iy >= info.mh)
		{
			continue;
		}
		iwData = 1280;
		if (x / c320 == info.bw - 1)
		{
			iwData = (info.mw - x) * 4;
		}
		if (!_data)
		{
			memcpy(&ptr[h * info.mw], &data[h * c320], iwData);
		}
		else
		{
			memcpy(&_data[iy * info.mw + x], &data[h * c320], iwData);
			uchar* c;
			for (int w = iwData / 4 - 1; w >= 0; --w)
			{
				c = (uchar*)&_data[iy * info.mw + x + w];
				std::swap(c[0], c[2]);
			}
			if ((h % _smalltimes) == 0)
			{
				for (int w = 0; w < c320; w += _smalltimes)
				{
					ix = x + w;
					if (x / c320 == info.bw - 1 && ix >= info.mw)
					{
						continue;
					}
					ptr[(h / _smalltimes) * (info.mw / _smalltimes) + w / _smalltimes] = data[h * c320 + w];
				}
			}
		}
	}

// 	uchar* c;
// 	for (int i = c320 * c240 - 1; i >= 0; --i)
// 	{
// 		c = (uchar*)&data[i];
// 		std::swap(c[0], c[2]);
// 	}
// 	save2png(cpp::toString("%d/%s/%d-%02d-%02d.jpg", _id, s_folderCut, _id, iBlock % info.bw, iBlock / info.bw).c_str(), (uchar*)data, c320, c240);
	_texture->Unlock();
	textureCut->Unlock();
	if(save)
	{
		textureCut->SaveToFile(toString("%s%d/%s/%d-%02d-%02d.jpg", _path.c_str(), _id, s_folderCut, _id, iBlock % info.bw, iBlock / info.bw).c_str(), gge::GGE_IMAGE_FORMAT::IMAGE_JPG);
	}
	textureCut->Release();
	return true;
}



bool cMapWriter::readMask(int iMask)
{
	auto& mask = _m.masks[iMask];
	if (mask.x < 0 || mask.y < 0)
	{
//		ccc_delete_array(mask.masks);
		return true;
	}
	if (!mask.decode((uchar*)_m.ptr + *(_m.maskOffs + iMask)))
	{
		return true;
	}
	const auto& info = _m.info;
	int x, y;
	uint* ptr = _texture->Lock(true, mask.x / _smalltimes, mask.y / _smalltimes, std::min(mask.w, info.mw - mask.x) / _smalltimes, std::min(mask.h, info.mh - mask.y) / _smalltimes);
	uint* ptrMask = _textureMask->Lock(false, mask.x / _smalltimes, mask.y / _smalltimes, std::min(mask.w, info.mw - mask.x) / _smalltimes, std::min(mask.h, info.mh - mask.y) / _smalltimes);
	uchar* c;
	for (int h = mask.h - 1; h >= 0; --h)
	{
		for (int w = mask.w - 1; w >= 0; --w)
		{
			if (mask.decodes[h * mask.w + w])
			{
				x = mask.x + w;
				y = mask.y + h;
				if (x >= info.mw)
				{
					continue;
				}
				if (y >= info.mh)
				{
					continue;
				}
				if (_data)
				{
					_dataMask[y * info.mw + x] = _data[y * info.mw + x];
					c = (uchar*)&_dataMask[y * info.mw + x];
					c[3] = 0x88;
				}
				if ((x % _smalltimes) || (h % _smalltimes))
				{
					continue;
				}
// 				c = (uchar*)&ptr[(h / smalltimes) * (info.mw / smalltimes) + w / smalltimes];
// 				std::swap(c[0], c[2]);
// 				c[3] = 0x88;
// 				ptrMask[(h / smalltimes) * (info.mw / smalltimes) + w / smalltimes] = *(uint*)c;
				ptrMask[(h / _smalltimes) * (info.mw / _smalltimes) + w / _smalltimes] = ptr[(h / _smalltimes) * (info.mw / _smalltimes) + w / _smalltimes];
					
			}
		}
	}
	_textureMask->Unlock();
	_texture->Unlock();
//	ccc_delete_array(mask.masks);
	return true;
}


bool cMapWriter::writeMask(int iBlock)
{
	const auto& info = _m.info;
	int x = (iBlock % info.bw) * c320;
	int y = (iBlock / info.bw) * c240;
	uint* ptr = _texture->Lock(false, x / _smalltimes, y / _smalltimes, std::min(c320, info.mw - x) / _smalltimes, std::min(c240, info.mh - y) / _smalltimes);
	uint* ptrMask = _textureMask->Lock(true, x / _smalltimes, y / _smalltimes, std::min(c320, info.mw - x) / _smalltimes, std::min(c240, info.mh - y) / _smalltimes);

	uint* ptrCut = new uint[c320 * c240];
	memset(ptrCut, 0, c320 * c240 * 4);
	int iy, iwData;
	for (int h = 0; h < c240; ++h)
	{
		iy = y + h;
		if (y / c240 == info.bh - 1 && iy >= info.mh)
		{
			continue;
		}
		iwData = c320;
		if (x / c320 == info.bw - 1)
		{
			iwData = (info.mw - x);
		}
		if ((h % _smalltimes) == 0)
		{
			memcpy(&ptr[(h / _smalltimes) * (info.mw / _smalltimes)], &ptrMask[(h / _smalltimes) * (info.mw / _smalltimes)], iwData / _smalltimes * 4);
		}

		if (_data)
		{
			memcpy(&ptrCut[h * c320], &_dataMask[iy * info.mw + x], iwData * 4);
		}
		else
		{
			memcpy(&ptrCut[h * c320], &ptr[h * info.mw], iwData * 4);
			uchar* c;
			for (int w = iwData - 1; w >= 0; --w)
			{
				uint& c4 = ptrCut[h * c320 + w];
				if (c4)
				{
					c = (uchar*)&c4;
					std::swap(c[0], c[2]);
					c[3] = 0x88;
				}
			}

		}
	}
	_texture->Unlock();
	_textureMask->Unlock();
	save2png(toString("%s%d/%s/%d-%02d-%02d.png", _path.c_str(), _id, s_folderMask, _id, iBlock % info.bw, iBlock / info.bw).c_str(), (uchar*)ptrCut, c320, c240);
	delete[] ptrCut;
	return true;

}



bool cMapWriter::writeObstable()
{
	const auto& info = _m.info;
	gge::ggeTexture* textureObstacle = gge::Texture_Create(info.bw * 16, info.bh * 12);
//	textureObstacle->FillColor(0xffffffff);
	uint* ptr = textureObstacle->Lock();
	memset(ptr, 0xFF, info.bw * 16 * info.bh * 12 * 4);
	for (int k = 0; k < info.bh * 12; ++k)
	{
		for (int i = 0; i < info.bw * 16; ++i)
		{
			if (_m.isObstacle(i, k))
			{
				ptr[k * info.bw * 16 + i] = 0xff000000;
			}
		}
	}
	textureObstacle->Unlock();
	textureObstacle->SaveToFile(toString("%s%d/%d.bmp", _path.c_str(), _id, _id).c_str(), gge::GGE_IMAGE_FORMAT::IMAGE_BMP);
	textureObstacle->Release();
	return true;
}




void cMapWriter::update(float delta)
{
// 	float scroll = gge::Input_GetMouseWheel();
// 	float scale = getScale();
// 	if (scroll != 0)
// 	{
// 		scale *= 1 - scroll * 0.1f;
// 		scale = std::max(0.1f, std::min(1.0f, scale));
// 		setScale(scale);
// 	}
	const auto& info = _m.info;
	if (_flag == 0)
	{
		if (_iblock < info.bw * info.bh)
		{
			if(readJPG(_iblock, true))
			{
				_labelDesc->setString(toString("导出切图 %d:%d", ++_iblock, info.bw * info.bh));
			}
			else 
			{
				_m.clear();
				std::string name = _path + toString("%d.map", _id);
				ccc_move(name, "error/");
				_isFinish = true;
				return;
			}
		}
		else
		{
			_labelDesc->setString("导出大图jpg.很久");
			++_flag;
		}
	}
	else if (_flag == 1)
	{
		std::string str = toString("%s%d/%d.jpg", _path.c_str(), _id, _id);
		if (_data)
		{
			save2png(str.c_str(), (uchar*)_data, info.mw, info.mh);
		}
		else
		{
			_texture->SaveToFile(str.c_str(), gge::GGE_IMAGE_FORMAT::IMAGE_PNG);
		}
		_labelDesc->setString("导出大图tga.很久");
		//////////////////////////////////////////////////////////////////////////
#if 0
		_texture->Release();
		_textureMask->Release();
		_labelDesc->setString("导出完毕");
		if (_data) {
			delete[] _data;
			delete[] _dataMask;
		}
		_isFinish = true;
		return;
#endif
		//////////////////////////////////////////////////////////////////////////
		++_flag;
	}
	else if (_flag == 2)
	{
		std::string str = toString("%s%d/%d.tga", _path.c_str(), _id, _id);
		if (_data)
		{
			save2tga(str.c_str(), (uint*)_data, info.mw, info.mh);
		}
		else
		{
			_texture->SaveToFile(str.c_str(), gge::GGE_IMAGE_FORMAT::IMAGE_TGA);
		}
		//////////////////////////////////////////////////////////////////////////
#if 0
		_texture->Release();
		_textureMask->Release();
		_labelDesc->setString("导出完毕");
		if (_data) {
			delete[] _data;
			delete[] _dataMask;
		}
		_isFinish = true;
		return;
#endif
		//////////////////////////////////////////////////////////////////////////

		cSprite::setTexture(_textureMask);
		++_flag;
	}
	else if (_flag == 3)
	{
		if (_imask < _m.masksCount)
		{
			readMask(_imask);
			_labelDesc->setString(toString("分析遮罩 %d:%d", ++_imask, _m.masksCount));
		}
		else
		{
			_iblock = 0;
		//	_texture->FillColor(0);
			auto ptr = _texture->Lock();
			memset(ptr, 0, _texture->GetWidth() * _texture->GetHeight() * 4);
			_texture->Unlock();
			cSprite::setTexture(_texture);
			++_flag;
		}
	}
	else if (_flag == 4)
	{
		if (_iblock < info.bw * info.bh)
		{
			writeMask(_iblock);
			_labelDesc->setString(toString("导出遮罩 %d:%d", ++_iblock, info.bw * info.bh));
		}
		else
		{
			_labelDesc->setString("导出大遮罩.很久");
			++_flag;
		}
	}
	else if (_flag == 5)
	{
		std::string str = toString("%s%d/%d.png", _path.c_str(), _id, _id);
		if (_data)
		{
			save2png(str.c_str(), (uchar*)_dataMask, info.mw, info.mh);
			delete[] _data;
			delete[] _dataMask;
		}
		else
		{
			uint* ptr = _texture->Lock();
			uchar* c;
			for (int i = info.mw * info.mh - 1; i >= 0; --i)
			{
				if (ptr[i])
				{
					c = (uchar*)&ptr[i];
					std::swap(c[0], c[2]);
					c[3] = 0x88;
				}
			}
			save2png(str.c_str(), (uchar*)ptr, info.mw, info.mh);
			_texture->Unlock();
		}
		_texture->Release();
		_textureMask->Release();
		++_flag;
	}
	else if (_flag == 6)
	{
		_labelDesc->setString("导出完毕");
		if (_m.loadObstacles())
		{
			writeObstable();
		}
		_isFinish = true;
	}
}

